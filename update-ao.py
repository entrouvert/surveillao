#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import datetime
import site

# virtualenv
site.addsitedir("/var/vhosts/ao.entrouvert.org/virtualenv/lib/python2.6/site-packages")
# django init
sys.path.append('/var/vhosts/ao.entrouvert.org/virtualenv/')
sys.path.append('/var/vhosts/ao.entrouvert.org/virtualenv/surveillao/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from surveillao.base.models import Feed
for feed in Feed.objects.all():
    print '%s update %s ...' % (datetime.datetime.now(), feed),
    try:
        added = feed.read()
    except:
        print 'error'
    else:
        if added == -1:
            print 'postponed'
        else:
            print 'done (%d new items)' % added

