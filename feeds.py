# -*- encoding: utf-8 -*-

from django.contrib.syndication.views import Feed
from base.models import Item
from datetime import datetime, timedelta

class AoFeed(Feed):
    title = "Appels d'offres pour EO"
    link = "http://www.entrouvert.com/"
    description = "Aggregation de flux RSS filtrés (relatifs à des AO)"
    ttl = 360

    def items(self):
        # list items added last 3 days
        last_add = datetime.now() + timedelta(-3)
        return Item.objects.filter(add_date__gte=last_add)

    def item_title(self, item):
        title = '[' + item.source.__unicode__() + '] ' + item.title
        if item.mark:
            return '[!] ' + title
        else:
            return title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return item.link


class AoFeedMark(AoFeed):
    title = "Appels d'offres MARQUÉS"

    def items(self):
        last_add = datetime.now() + timedelta(-3)
        return Item.objects.filter(mark=True, add_date__gte=last_add)

