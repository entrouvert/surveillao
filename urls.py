from django.conf.urls.defaults import *
from surveillao.feeds import AoFeed, AoFeedMark

# enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^surveillao/', include('surveillao.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    (r'^admin/', include(admin.site.urls)),
    (r'^all.atom$', AoFeed()),
    (r'^mark.atom$', AoFeedMark()),
)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()

