# wsgi application for "surveillao"

import os, sys

sys.path.append('/var/vhosts/ao.entrouvert.org/virtualenv/')
sys.path.append('/var/vhosts/ao.entrouvert.org/virtualenv/surveillao/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()

