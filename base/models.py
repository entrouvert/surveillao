from django.db import models
import feedparser
import datetime
import re

UNKNOWN_DATE = datetime.datetime(1970,1,1)

class Feed(models.Model):
    url = models.URLField()
    name = models.CharField(max_length=200)
    shortname = models.CharField(max_length=20, blank=True)
    regex = models.CharField(max_length=500, default='.*(web|internet|logiciel|informati|grc|citoyen)')
    regex_exclude = models.CharField(max_length=500, default='', blank=True)
    add_date = models.DateTimeField(auto_now_add=True)
    parsed_date = models.DateTimeField(default=UNKNOWN_DATE)
    duration = models.IntegerField(help_text='in minutes', default=30)

    def __unicode__(self):
        if self.shortname != '':
            return u'%s' % self.shortname
        else:
            return u'%s' % self.name

    def read(self, force=False):
        if not force:
            # abort if last read is recent (<self.duration)
            elapsed = datetime.datetime.now() - self.parsed_date
            if ((elapsed.days*24*3600+elapsed.seconds)/60) < self.duration:
                return -1
        added = 0
        feed = feedparser.parse(self.url)
        re_in = re.compile(self.regex, re.IGNORECASE)
        if self.regex_exclude != '':
            re_out = re.compile(self.regex_exclude, re.IGNORECASE)
        else:
            re_out = None
        for e in feed.entries:
            if re_in.match(e.title) or re_in.match(e.description):
                if re_out != None:
                    if re_out.match(e.title) or re_out.match(e.description):
                        continue
                d = e.date_parsed
                dt = datetime.datetime(d[0],d[1],d[2],d[3],d[4],d[5])
                try:
                    Item(link=e.link,
                        title=e.title,
                        published_date=dt,
                        expire_date=UNKNOWN_DATE,
                        description=e.description,
                        source=self).save()
                    added += 1
                except:
                    pass # certainly a duplicate item
        self.parsed_date = datetime.datetime.now()
        self.save()
        return added

class Item(models.Model):
    # from feed
    link = models.URLField(unique=True)
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=500)
    published_date = models.DateTimeField()
    # user content
    mark = models.NullBooleanField(default=None)
    expire_date = models.DateTimeField(default=UNKNOWN_DATE)
    comments = models.TextField(max_length=1000, blank=True)
    # system info
    source = models.ForeignKey(Feed)
    add_date = models.DateTimeField(auto_now_add=True)

    def direct_link(self):
        return '<a href="%s">direct<br />link</a>' % self.link
    direct_link.short_description = ''
    direct_link.allow_tags = True
    direct_link.admin_order_field = 'title'

    def expire_date_nice(self):
        if self.expire_date == UNKNOWN_DATE:
            return 'unknown'
        else:
            return self.expire_date
    expire_date_nice.short_description = 'expire date'
    expire_date_nice.allow_tags = True
    expire_date_nice.admin_order_field = 'expire_date'


