from base.models import Feed, Item
from django.contrib import admin

class FeedAdmin(admin.ModelAdmin):
    list_display = ['shortname', 'name', 'url', 'parsed_date']
    list_display_links = ['shortname', 'name']
    ordering = ['parsed_date']
    actions = ['read_feed']

    def read_feed(modeladmin, request, queryset):
        for feed in queryset:
            feed.read()
    read_feed.short_description = "Read selected feeds"

admin.site.register(Feed, FeedAdmin)


class ItemAdmin(admin.ModelAdmin):
    list_per_page = 20
    actions_on_top = True
    actions_on_bottom = True
    save_on_top = True
    readonly_fields = ['link', 'source']
    list_display = ['mark', 'title', 'direct_link' ,'expire_date_nice',
                       'published_date', 'source']
    list_display_links = ['title']
    list_filter = ['mark', 'source', 'expire_date']
    list_editable = ['mark']
    ordering = ['-add_date']
    date_hierarchy = 'expire_date'
    actions = ['mark_item', 'unmark_item']
    search_fields = ['title', 'description', 'comments', 'link']

    def mark_item(modeladmin, request, queryset):
        queryset.update(mark=True)
    mark_item.short_description = "Mark selected items"

    def unmark_item(modeladmin, request, queryset):
        queryset.update(mark=False)
    unmark_item.short_description = "Unmark selected items"

admin.site.register(Item, ItemAdmin)

